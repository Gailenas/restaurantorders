# My project's README
# Final

Logins:

admin@admin.admin password: adminadmin
user@user.user password useruser

Frameworks used:
Laravel, Bootstrap, jQuery

Database used:
SQLite

About website:
Main functions you can find on main website.
Functions like cart, old orders, user info change you can find in dropdown.

How to start with this system:
In terminal use these commands: 
 1. Make command: git clone https://Gailenas@bitbucket.org/Gailenas/restaurantOrders.org
In terminal move to main folder of project.
 2. Make command: php artisan key: generate
 3. Make command: php artisan serve
In browser open: localhost:8000