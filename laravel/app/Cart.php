<?php

namespace App;



class Cart
{
    public static function get()
    {
    	$cart = session('cart', []);

    	return $cart;
    }

    public static function save($cart)
    {
    	session() -> put('cart', $cart);
    }

    public static function totalBrutto()
    {
    	$cart = static::get();
    	$products = static::getProducts();
    	$sum = 0;

    	foreach ($products as $product) {
    		$sum += $cart[$product->id]*$product->price;
    	}

    	return $sum;
    }

    public static function totalNetto()
    {
    	$cart = static::get();
    	$products = static::getProducts();
    	$sum = 0;

    	foreach ($products as $product) {
    		$sum += $cart[$product->id]*$product->getPriceNetto();
    	}

    	return $sum;
    }

    public static function addItem($id, $amount)
    {
    	$cart = static::get();

    	if (array_key_exists($id, $cart))
        {
            $cart[$id]=$cart[$id]+$amount;
        }
        else
        {
            $cart[$id]=$amount;
        }

        static::save($cart);
    }

    public static function removeItem($id)
    {
    	$cart = static::get();
        if (array_key_exists($id, $cart)) 
        {
            unset($cart[$id]);
        }

        static::save($cart);
    }

    public static function getProducts()
    {
 		$cart = static::get();

        $products = Product::whereIn('id', array_keys($cart))->get();   

        return $products;	
    }
}