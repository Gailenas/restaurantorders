<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class MeniuListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return view('products.meniu', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:products|max:40',
            'description' => 'required',
            'price' => 'required|numeric',
        ]);


        $prod = Product::create($request->all());

        $path = $request->file('image');//

        if ($path !== null) {
            $path = $path->store('public');
            $prod->update(['image' => $path]);
        }

        return redirect()
            ->route('meniu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        return view('products.product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('products.edit', compact('product'));

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:products,name,'.$id.'|max:25',
            'description' => 'required',
            'price' => 'required|numeric',
        ]);

        $product = Product::find($id);
        $product->update($request->all());

        $path = $request->file('image');//
        if ($path !== null) {
            $path = $path->store('public');
            $product->update(['image' => $path]);
        }

        return redirect()
            ->route('meniu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->route('meniu.index')
        ->with('success','Item deleted successfully');
    }
}
