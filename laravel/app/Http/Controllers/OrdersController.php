<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use \App\ProductOrder;
use \App\User;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->admin == 1)
        $order = Order::all();
        else
        $order = Auth::user()->orders;


        return view('orders.orders', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = New Order();
        $total = \App\Cart::totalBrutto();
        $products = \App\Cart::getProducts();
        $cart = \App\Cart::get();

        $order->user_id = Auth::user()->id;
        $order->total = $total;
        $order->save();

        foreach ($products as $product)
        {
            $order->products()->attach($product->id, ['price'=>$product->price, 'quantity' => $cart[$product->id]]);
        }

        $request->session()->forget('cart');

        // isvalyt is sesijos krepseli
        // peradresuoti useri i uzsakymu sarasa
//        return redirect('orders.index', compact('order'));
        return redirect()  ->route('orders.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $order = Order::find($id);
        $products = $order -> productOrders;


        return view('orders.order')->with( compact('products', 'order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
