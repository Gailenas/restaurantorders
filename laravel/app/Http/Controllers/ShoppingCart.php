<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ShoppingCart extends Controller
{   public function store(Request $request)
    {
        $productId = $request->get('productId');
        $amount = (int)$request->get('amount');

        \App\Cart::addItem($productId, $amount);

        return redirect()->route('cart.index');
    }

    public function index(Request $request)
    {
        $cart = \App\Cart::get();

        $products = \App\Cart::getProducts();

        $totalBrutto = \App\Cart::totalBrutto();

        $totalNetto = \App\Cart::totalNetto();


        return view('cart.cart', compact('products', 'cart', 'totalBrutto', 'totalNetto'));
    }

    public function destroy($id)
    {
        \App\Cart::removeItem($id);

        return redirect() -> route('cart.index');
    }
}
