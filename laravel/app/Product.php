<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','description','price', 'image'];

    public function getPriceNetto()
    {

    	return $this->price * 1.21;
    }
}


