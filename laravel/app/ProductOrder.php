<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrder extends Model
{
    protected $fillable = [
        'order_id', 'product_id', 'quantity', 'price',
    ];

    protected $table = 'order_product';

    public function products()
    {
        return $this->belongsTo(Product::class);
    }
}
