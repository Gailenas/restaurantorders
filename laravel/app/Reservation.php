<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['user', 'number', 'date', 'spaces', 'comment'];
}
