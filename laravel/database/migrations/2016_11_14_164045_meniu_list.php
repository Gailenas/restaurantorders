<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MeniuList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('products', function (Blueprint $table) {
        $table->increments('id')->nullable();
        $table->string('name', 16)->unique()->nullable();
        $table->string('description')->nullable();
        $table->integer('price')->nullable();
        $table->string('image')->nullable();
        $table->string('category')->nullable();
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
