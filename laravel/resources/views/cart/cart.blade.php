@extends('layouts.app')

@section('content')
    <a href="/meniu"><button>Meniu</button></a>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>ID:</th>
                                <th>Name:</th>
                                <th>Description:</th>
                                <th>Image:</th>
                                <th>Price:</th>
                                <th>Quantity:</th>
                                <th>Price Netto:</th>
                                <th></th>
                            </tr>
                                @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <td>@if($product->image == null)
                                    <span>[Nuotraukos nera]</span>
                                    @else
                                    <img src="{{ Storage::url($product->image) }}" alt="" style="max-width: 200px;">
                                    @endif
                                </td>
                                <td>{{ $product->getPriceNetto() }}</td>
                                <td>{{ $cart[$product->id] }}</td>
                                <td>{{ $cart[$product->id]*$product->getPriceNetto()}}</td>
                                <td>
                                    <div>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'route' => ['cart.destroy', $product->id]
                                            ]) !!}
                                            {!! Form::submit('delete', ['class' => 'button']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        <table class="table">
                            <tr>
                                <th>Total brutto:</th>
                                <th>Total Netto:</th>
                            </tr>
                            <tr>
                                <th>{{ $totalBrutto }}</th>
                                <th>{{ $totalNetto }}</th>
                            </tr>
                        </table>
                        <div>
                            <form action="{{ route('orders.store') }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-success">BUY</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
