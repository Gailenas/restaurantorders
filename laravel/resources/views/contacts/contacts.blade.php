@extends('layouts.app')

@section('content')
<div class="kontaktai">
    <h1>Fresh food</h1>
        <div class="row">
            <div class="col-md-6">
                <h3>You can contact us with:</h3>
                <p>Phone: <a href="tel:+37060497078">+37060497078</a></p>
                <p>E-Mail: <a href="mailto:gailenas@yahoo.com">gailenas@yahoo.com</a></p>
            </div>
            <div class="col-md-6">
                <h3>You can find us at:</h3>
                <p>Svitrigailos 100</p>
                <p>Vilnius</p>
                <p>Lithuania</p>
            </div>
        </div>
        <div id="map">
            <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAS1lZyqQwX-fW2PMy9yHf_qJpAeyAEWGc'></script>
            <div style='overflow:hidden;height:400px;width:100%;'>
            <div id='gmap_canvas' style='height:400px;width:100%;'></div>
            <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
            </div> <a href='https://embedmaps.org/'>google map for my website</a>
            <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=ae351d77536900c1f6b414fc35514c27f449b306'></script>
            <script type='text/javascript'>function init_map(){var myOptions = {zoom:12,center:new google.maps.LatLng(54.6738996,25.268758299999945),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(54.6738996,25.268758299999945)});infowindow = new google.maps.InfoWindow({content:'<strong>Fresh Food</strong><br>svitrigailos<br> <br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
        </div>
</div>
@endsection
