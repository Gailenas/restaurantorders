@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th>Order ID:</th>
                        <th>User_ID:</th>
                        <th>Total:</th>
                    </tr>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->user_id }}</td>
                        <td>{{ $order->total }}</td>
                    </tr>
                </table>
                <table class="table table-hover">
                    <tr>
                        <th>Product ID:</th>
                        <th>Order ID:</th>
                        <th>Quantity:</th>
                    </tr>
                    @foreach($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->order_id }}</td>
                        <td>{{ $product->quantity }}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection