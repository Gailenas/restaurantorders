@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th>Order ID:</th>
                        <th>User ID:</th>
                        <th>Total price:</th>
                    </tr>
                    @foreach ($order as $one)
                        <tr style="cursor:pointer" onclick="window.location.href ='{{ route('orders.show', $one->id) }}';">
                            <td>{{ $one->id }}</td>
                            <td>{{ $one->user_id }}</td>
                            <td>{{ $one->total }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>


@endsection