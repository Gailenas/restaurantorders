@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
         <a href="/meniu"><button>To meniu list:</button></a>
    </div>
</div>
<div>
    @foreach ($errors->all() as $message)
        <div class="alert alert-danger">
            {{$message}}
        </div>
    @endforeach
    {!! Form::open(array('route' => 'meniu.store', 'files' => true)) !!}
        <strong>Product:</strong>
        {!! Form::text('name', '',['class' => 'form-control']) !!}
        <strong>Description:</strong>
        {!! Form::text('description', '',['class' => 'form-control']) !!}
        <strong>Price without VAT:</strong>
        {!! Form::text('price', '',['class' => 'form-control']) !!}
        <strong>Image:</strong>
        {!! Form::file('image',['class' => 'form-control']) !!}
        <button type="submit">Submit</button>
    {!! Form::close() !!}
</div>
@endsection