@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="/meniu"><button>To meniu list:</button></a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                @foreach ($errors->all() as $message)
                    <div class="alert-danger">
                        {{$message}}
                    </div>
                @endforeach
                {{--{!! Form::open(array('route' => 'meniu.update', 'files' => true)) !!}--}}
                {!! Form::model($product, array('method' => 'PUT', 'url' => route('meniu.update',  ['meniu' => $product->id]), 'files' => true)) !!}
                    <strong>Product:</strong>
                    {!! Form::text('name', $product->product,['class' => 'form-control']) !!}
                    <strong>Description:</strong>
                    {!! Form::text('description', $product->description,['class' => 'form-control']) !!}
                    <strong>Price:</strong>
                    {!! Form::text('price', $product->price,['class' => 'form-control']) !!}
                    <img src="{{ Storage::url($product->image) }}">
                    {!! Form::file('image')!!}
                    <button type="submit">Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection