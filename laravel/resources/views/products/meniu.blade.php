@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- @if (Auth::check())
        <a href="/cart"><button>Cart:</button></a>
        @endif -->
        @if(Auth::check() && Auth::user()->isAdmin())
            <a href="/meniu/create"><button>Create new item:</button></a>
        @endif
    </div>
</div>
@if(Auth::check() && Auth::user()->isAdmin())
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <th>Product:</th>
                            <th>Description:</th>
                            <th>Price:</th>
                            <th>Image:</th>
                        </tr>
                        @foreach ($products as $product)
                            <tr style="cursor:pointer" onclick="window.location.href ='{{ route('meniu.show', $product->id) }}';">
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->price }}</td>
                                <td>@if($product->image == null)
                                        <span>[Nuotraukos nera]</span>
                                    @else
                                        <img src="{{ Storage::url($product->image) }}" alt="" style="max-width: 300px;">
                                    @endif
                                </td>
                                <td>
                                    <button>
                                        <a href="{{ route('meniu.edit', $product->id) }}">edit</a>
                                    </button>
                                    <div>
                                        {!! Form::open([
                                            'method' => 'DELETE',
                                            'route' => ['meniu.destroy', $product->id]
                                        ]) !!}
                                        {!! Form::submit('delete', ['class' => 'button']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
        @else
    <div class="row">
        <div class="col-sm-12">
        @foreach ($products as $product)
            <a href="{{ route('meniu.show', $product->id) }}" class="col-lg-4 col-md-6 col-sm-12">
                <div class="klasele">
                    <span>{{ $product->name }}</span>
                    <img src="{{ Storage::url($product->image) }}" class="img-responsive center-cropped" >
                    <span>{{ $product->description }}</span>
                    <br>
                    <span>{{ $product->price }} EUR</span>
                </div>
            </a>
        @endforeach
        </div>
        @endif
    </div>
</div>
@endsection