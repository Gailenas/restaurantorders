@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="/meniu"><button>Meniu:</button></a>
        @if (Auth::check())
        <a href="/cart"><button>Cart:</button></a>
        @endif
    </div>
</div>
<div class="row kontaktai">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <th>ID:</th>
                        <th>Name:</th>
                        <th>Description:</th>
                        <th>Price:</th>
                        <th>Image:</th>
                    </tr>
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->price }}</td>
                        <td>@if($product->image == null)
                                <span>[Nuotraukos nera]</span>
                            @else
                                <img src="{{ Storage::url($product->image) }}" alt="" style="max-width: 200px;">
                            @endif</td>
                        <td>
                        @if(Auth::check() && Auth::user()->isAdmin())
                        <td>
                            <button>
                                <a href="{{ route('meniu.edit', $product->id) }}">edit</a>
                            </button>
                            <div>
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['meniu.destroy', $product->id]
                                ]) !!}
                                {!! Form::submit('delete', ['class' => 'button']) !!}
                                {!! Form::close() !!}
                            </div>
                            @endif
                            @if (Auth::check())
                                {{ Form::open(array("method"=>"POST", 'route' => 'cart.store')) }}
                                {{ Form::hidden('productId', $product->id) }}
                                {{ Form::text('amount', '1', ['class' => 'form-control']) }}
                                {{ Form::submit('To cart') }}
                                {{ Form::close() }}
                        </td>
                        @endif
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
