@extends('layouts.app')

@section('content')
<div class="kontaktai">
	@foreach ($errors->all() as $message)
    <div class="alert alert-danger">
                {{$message}}
    </div>
        @endforeach
        {!! Form::open(array('route' => 'table.store', 'files' => true)) !!}
            <strong>User:</strong>
            {!! Form::text('user', '',['class' => 'form-control']) !!}
            <strong>Number:</strong>
            {!! Form::text('number', '',['class' => 'form-control']) !!}
            <strong>Date:</strong>
            {!! Form::text('date', '',['class' => 'form-control']) !!}
            <strong>Time:</strong>
            {!! Form::text('time', '',['class' => 'form-control']) !!}
            <strong>Table size:</strong>
            {!! Form::text('spaces', '',['class' => 'form-control']) !!}
            <strong>Comment:</strong>
            {!! Form::text('comment', '',['class' => 'form-control']) !!}
            <button type="submit">Submit</button>
            {!! Form::close() !!}
    <script>
      $( function() {
        $( "[name=date]" ).datepicker({dateFormat:'yy-mm-dd'});
      } );
     </script>
 </div>
@endsection