@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <th>User name:</th>
                        <th>User number:</th>
                        <th>Date:</th>
                        <th>Table for:</th>
                    </tr>
                    @foreach ($reservations as $reservation)
                        <tr style="cursor:pointer" onclick="window.location.href ='{{ route('meniu.show', $reservation->id) }}';">
                            <td>{{ $reservation->user }}</td>
                            <td>{{ $reservation->number }}</td>
                            <td>{{ $reservation->date }}</td>
                            <td>{{ $reservation->spaces }}</td>
                            <td>
                                <button>
                                    <a href="{{ route('meniu.edit', $reservation->id) }}">edit</a>
                                </button>
                                <div>
                                    {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['table.destroy', $reservation->id]
                                    ]) !!}
                                    {!! Form::submit('delete', ['class' => 'button']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection