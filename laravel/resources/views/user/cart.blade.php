@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table">
            <tr>
                <th>ID:</th>
                <th>Name:</th>
                <th>Description:</th>
                <th>Price:</th>
                <th>Qty:</th>
            </tr>
                @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->price }}</td>
                <td>{{$cart[$product->id]}}</td>
                <td>

                    <div>
                            {!! Form::open([
                                'method' => 'DELETE',
                                'route' => ['cart.destroy', $product->id]
                            ]) !!}
                            {!! Form::submit('delete', ['class' => 'button']) !!}
                            {!! Form::close() !!}
                        </div>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@endsection
