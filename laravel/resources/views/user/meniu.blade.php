@extends('layouts.app')

@section('content')

<div class="kontaktai">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-hover">
                        <tr>
                            <th>Product:</th>
                            <th>Description:</th>
                            <th>Price:</th>
                            <th>Image:</th>
                        </tr>
                        @foreach ($products as $product)
                            <tr style="cursor:pointer" onclick="window.location.href ='{{ route('meniu.show', $product->id) }}';">
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->price }}</td>
                                <td>@if($product->image == null)
                                        <span>[Nuotraukos nera]</span>
                                    @else
                                        <img src="{{ Storage::url($product->image) }}" alt="">
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection