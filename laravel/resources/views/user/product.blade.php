@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>ID:</th>
                                <th>Name:</th>
                                <th>Description:</th>
                                <th>Price:</th>
                                <th>Nuotrauka:</th>
                            </tr>
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>
                                        @if($product->categoryDep)
                                            {{ $product->categoryDep->category }}
                                        @endif
                                    </td>
                                    <td>
                                        <button>
                                            <a href="{{ route('meniu.edit', $product->id) }}">edit</a>
                                        </button>
                                        <div>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'route' => ['meniu.destroy', $product->id]
                                            ]) !!}
                                            {!! Form::submit('delete', ['class' => 'button']) !!}
                                            {!! Form::close() !!}
                                        </div>
                                            {{ Form::open(array("method"=>"POST", 'route' => 'cart.store')) }}
                                            {{ Form::hidden('productId', $product->id) }}
                                            {{ Form::text('amount', '1', ['class' => 'form-control']) }}
                                            {{ Form::submit('To cart') }}
                                            {{ Form::close() }}
                                    </td>
                                </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
