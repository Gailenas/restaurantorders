@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="/meniu"><button>To meniu list:</button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @foreach ($errors->all() as $message)
                            <div class="alert-danger">
                                {{$message}}
                            </div>
                        @endforeach
                            <strong>Name:</strong>
                            {!! Form::text('name', $user->name,['class' => 'form-control']) !!}
                            <strong>Surname:</strong>
                            {!! Form::text('surname', $user->surname,['class' => 'form-control']) !!}
                            <strong>E-mail:</strong>
                            {!! Form::text('email', $user->email,['class' => 'form-control']) !!}
                            <strong>Phone number:</strong>
                            {!! Form::text('phone', $user->phone,['class' => 'form-control']) !!}
                            <strong>Date of birth:</strong>
                            {!! Form::text('birth', $user->birth,['class' => 'form-control']) !!}
                            <strong>Location:</strong>
                            {!! Form::text('location', $user->location,['class' => 'form-control']) !!}
                            <strong>Country:</strong>
                            {!! Form::text('country', $user->country,['class' => 'form-control']) !!}
                            <strong>City:</strong>
                            {!! Form::text('city', $user->city,['class' => 'form-control']) !!}
                            <strong>Address:</strong>
                            {!! Form::text('address', $user->address,['class' => 'form-control']) !!}
                            <strong>ZIP code:</strong>
                            {!! Form::text('zip', $user->zip,['class' => 'form-control']) !!}
                            <button type="submit">Submit</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection