@extends('layouts.app')

@section('content')
<div class="kontaktai">
    <div class="row">
        <div id="no-more-tables">
            <table class="col-md-12 table-bordered table-striped table-condensed cf">
                <thead class="cf">
                    <tr>
                        <th>ID:</th>
                        <th>Name:</th>
                        <th>Surname:</th>
                        <th>E-mail:</th>
                        <th>Phone number:</th>
                        <th>Birthday:</th>
                        <th>Location:</th>
                        <th>Country:</th>
                        <th>City:</th>
                        <th>Address:</th>
                        <th>ZIP code:</th>
                        <th>Admin:</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td data-title="User ID">{{ $user->id }}</td>
                        <td data-title="Name">{{ $user->name }}</td>
                        <td data-title="Surname">{{ $user->surname }}</td>
                        <td data-title="Email">{{ $user->email }}</td>
                        <td data-title="Number">{{ $user->phone }}</td>
                        <td data-title="Birhday">{{ $user->birth }}</td>
                        <td data-title="Location">{{ $user->location }}</td>
                        <td data-title="Country">{{ $user->country }}</td>
                        <td data-title="City">{{ $user->city }}</td>
                        <td data-title="Address">{{ $user->address }}</td>
                        <td data-title="ZIP code">{{ $user->zip }}</td>
                        <td data-title="Admin if 1">{{ $user-> admin }}</td>
                        @if($user-> admin == 1)
                        @else
                        <td>{!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['users.destroy', $user->id]
                                ]) !!}
                            {!! Form::submit('delete', ['class' => 'button']) !!}
                            {!! Form::close() !!}
                        </td>
                        @endif
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
