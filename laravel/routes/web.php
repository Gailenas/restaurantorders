<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('contacts', function() {
	return view('contacts.contacts');
});
//Route::get('order', 'OrdersController@show')->name('orders.order');
Route::resource('table', 'ReservationTableController', ['only'=>['create', 'store']]);
Route::get('orders/store', 'OrdersController@store')->name('orders.store');

Route::group(['middleware' => ['auth', 'isAdmin']], function () {
    Route::resource('users', 'UserCheck');
    Route::resource('meniu', 'MeniuListController', ['except'=>['index', 'show']]);
    Route::resource('orders', 'OrdersController', ['except'=>['index', 'show', 'store']]);
    Route::resource('table', 'ReservationTableController', ['except'=>['create', 'store']]);
});

Route::group(['middleware' => ['auth']], function () {
    Route::resource('user', 'UserEdit');
    Route::resource('cart', 'ShoppingCart');
    Route::resource('orders', 'OrdersController', ['only'=>['index', 'show', 'store']]);
    Route::put('user', 'UserEdit@update')->name('user.update');
});

Route::resource('meniu', 'MeniuListController', ['only'=>['index', 'show']]);
Auth::routes();
Route::get('/home', 'HomeController@index');




